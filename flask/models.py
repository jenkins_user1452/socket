from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref

db = SQLAlchemy()


class PCModel(db.Model):
    __tablename__ = 'pc'

    id = db.Column(db.Integer, primary_key=True)
    cpu = db.Column(db.String(80), nullable=False)
    ram = db.Column(db.String(80), nullable=False)
    ssd = db.Column(db.String(80), nullable=False)
    price = db.Column(db.String(80), nullable=False)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), nullable=False)

    def __init__(self, cpu, ram, ssd, price):
        self.cpu = cpu
        self.ram = ram
        self.ssd = ssd
        self.price = price

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class CustomerModel(db.Model):
    __tablename__= 'customer'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    surname = db.Column(db.String(80), nullable=False)
    email = db.Column(db.String(80), nullable=False)
    pcs = db.relationship('pc', backref='customer', lazy=True)

    def __init__(self, name, surname, email):
        self.name = name
        self.surname = surname
        self.email = email

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class DealsModel(db.Model):
    __tablename__ = 'deals'
 
    id = db.Column(db.Integer, primary_key=True)
    pc_id = db.Column(db.Integer, db.ForeignKey('pc.id'), primary_key=True)
    customer_id = db.Column(db.Integer, db.ForeignKey('customer.id'), primary_key=True)

    def __init__(self, pc_id, customer_id):
        self.pc_id = pc_id
        self.customer_id = customer_id

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

