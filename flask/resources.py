from flask import jsonify
from flask_restful import reqparse, Resource
import json

from sqlalchemy.sql.operators import comma_op
from models import *


class PC(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('cpu', type=str, required=True, help='This field is required')
        parser.add_argument('ram', type=str, required=True, help='This field is required')
        parser.add_argument('ssd', type=str, required=True, help='This field is required')
        parser.add_argument('price', type=str, required=True, help='This field is required')

        data = parser.parse_args()

        try: 
            pc = PCModel(cpu=data['cpu'], ram=data['ram'], ssd=data['ssd'], price=data['price'])
            pc.save_to_db()
            
            return {'msg': 'PC added'}, 201
        except:
            return {'msg': 'Something went wrong'}, 400

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=str, required=True, help='This field is required')
        data = parser.parse_args()
        id = data['id']
        
        try:
            pc = PCModel.query.filter_by(id=id).first()
            db.session.delete(pc)
            db.session.commit()
            return {'msg': f'Deleted PC with id {id}'}
        except:
            return {'msg': f'Could not delete PC with id {id}'}


class Customer(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True, help='This field is required')
        parser.add_argument('surname', type=str, required=True, help='This field is required')
        parser.add_argument('email', type=str, required=True, help='This field is required')

        data = parser.parse_args()
        
        customer = CustomerModel(**data)
        customer.save_to_db()

        return {'msg': 'Customer added'}, 201

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=str, required=True, help='This field is required')
        data = parser.parse_args()
        id = data['id']
        
        try:
            pc = CustomerModel.query.filter_by(id=id).first()
            db.session.delete(pc)
            db.session.commit()
            return {'msg': f'Deleted Customer with id {id}'}
        except:
            return {'msg': f'Could not delete Customer with id {id}'}


class Deals(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('name', type=str, required=True, help='This field is required')
        parser.add_argument('surname', type=str, required=True, help='This field is required')
        parser.add_argument('email', type=str, required=True, help='This field is required')

        data = parser.parse_args()
        
        customer = CustomerModel(**data)
        customer.save_to_db()

        return {'msg': 'Customer added'}, 201

    def delete(self):
        parser = reqparse.RequestParser()
        parser.add_argument('id', type=str, required=True, help='This field is required')
        data = parser.parse_args()
        id = data['id']
        
        try:
            pc = CustomerModel.query.filter_by(id=id).first()
            db.session.delete(pc)
            db.session.commit()
            return {'msg': f'Deleted Customer with id {id}'}
        except:
            return {'msg': f'Could not delete Customer with id {id}'}

