from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


class Person(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), nullable=False)
    addresses = db.relationship('Address', backref='person', lazy=True)

class Address(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(80), nullable=False)
    person_id = db.Column(db.Integer, db.ForeignKey('person.id'), nullable=False)


mehdiabad = Address(email='kamran.karimov@mail.ru')
kamran = Person(name='kamran')
kamran.addresses.append(mehdiabad)

db.session.add(mehdiabad)
db.session.add(kamran)

db.session.commit()

user = Person.query.filter_by(name='kamran')
print(user.addresses)
print(user.addresses.count())

