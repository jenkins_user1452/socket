from flask import Flask
from flask_restful import Api
from resources import *


app = Flask(__name__)
app.config['SECRET_KEY']='qalxqalxulutorpaq'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///db.sqlite'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False


api = Api(app)

@app.before_first_request
def create_table():
    from models import db
    db.init_app(app)
    db.create_all()


api.add_resource(PC, '/api/pc')
api.add_resource(Customer, '/api/customer')
api.add_resource(Deals, '/api/deals')


if __name__ == '__main__':
    app.run(debug=True)
