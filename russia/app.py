from flask import Flask, request
from flask_restful import Api, Resource, reqparse, abort, fields, marshal_with
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)

class ComputerModel(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	cpu = db.Column(db.String(100), nullable=False)
	ram = db.Column(db.String(100), nullable=False)
	ssd = db.Column(db.String(100), nullable=False)
	price = db.Column(db.Integer, nullable=False)

	# def __repr__(self):
	# 	return f"ID: {id}\nVideo Name: {name}\nViews: {views}\nLikes: {likes}"

class CustomerModel(db.Model):
	id = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100), nullable=False)
	surname = db.Column(db.String(100), nullable=False)
	email = db.Column(db.String(100), nullable=False)

class DealModel(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    price = db.Column(db.Integer, nullable=False)
    pc = db.Column(db.Integer, db.ForeignKey('computer_model.id'), nullable=False)
    customer = db.Column(db.Integer, db.ForeignKey('customer_model.id'), nullable=False)


computer_post_args = reqparse.RequestParser()
computer_post_args.add_argument("cpu", type=str, help="CPU", required=True)
computer_post_args.add_argument("ram", type=str, help="RAM", required=True)
computer_post_args.add_argument("ssd", type=str, help="SSD", required=True)
computer_post_args.add_argument("price", type=int, help="Price", required=True)

client_post_args = reqparse.RequestParser()
client_post_args.add_argument("name", type=str, help="Name", required=True)
client_post_args.add_argument("surname", type=str, help="Surname", required=True)
client_post_args.add_argument("email", type=str, help="Email", required=True)

deal_post_args = reqparse.RequestParser()
deal_post_args.add_argument("id", type=int, help="ID", required=True)
deal_post_args.add_argument("price", type=int, help="Price", required=True)
deal_post_args.add_argument("pc", type=int, help="PC", required=True)
deal_post_args.add_argument("customer", type=int, help="Customer", required=True)

resource_fields_computer = {
	"id": fields.Integer,
	"cpu": fields.String,
	"ram": fields.String,
	"ssd": fields.String,
	"price": fields.Integer
}

resource_fields_client = {
	"id": fields.Integer,
	"name": fields.Integer,
	"surname": fields.Integer,
	"email": fields.Integer
}

resource_fields_deal = {
	"id": fields.Integer,
	"price": fields.Integer,
	"pc": fields.Integer,
	"customer": fields.Integer,
}


class Computer(Resource):
	@marshal_with(resource_fields_client)
	def post(self, pc_id):
		args = computer_post_args.parse_args()
		
		result = ComputerModel.query.filter_by(id=pc_id).first()
		if result:
			abort(409, message='PC is already have.')

		pc = ComputerModel(id=pc_id, cpu=args['cpu'], ram=args['ram'], ssd=args['ssd'], price=args['price'])
		db.session.add(pc)
		db.session.commit()
		return pc, 201

	@marshal_with(resource_fields_client)
	def delete(self, pc_id):

		result = ComputerModel.query.filter_by(id=pc_id).first()
		if not result:
			abort(404, message="Could not find with that ID")
		db.session.delete(result)
		db.session.commit()
		return result

api.add_resource(Computer, '/pc/<int:pc_id>')



class Client(Resource):
	@marshal_with(resource_fields_client)
	def post(self, client_id):
		args = client_post_args.parse_args()
		
		result = CustomerModel.query.filter_by(id=client_id).first()
		if result:
			abort(409, message='Customer is already have.')

		client = CustomerModel(id=client_id, name=args['name'], surname=args['surname'], email=args['email'])
		db.session.add(client)
		db.session.commit()
		return client, 201

	@marshal_with(resource_fields_client)
	def delete(self, client_id):

		result = CustomerModel.query.filter_by(id=client_id).first()
		if not result:
			abort(404, message="Could not find with that ID")
		db.session.delete(result)
		db.session.commit()
		return result

api.add_resource(Client, '/client/<int:client_id>')

class Deal(Resource):
	@marshal_with(resource_fields_deal)
	def get(self):

		result = DealModel.query.all()
		if not result:
			abort(404, message="Could not find with that ID")
		return result

	@marshal_with(resource_fields_deal)
	def post(self):
		args = deal_post_args.parse_args()
		
		result = DealModel.query.filter_by(id=args['id']).first()
		if result:
			abort(409, message='Deal is already have.')

		deal = DealModel(id=args['id'], price=args['price'], pc=args['pc'], customer=args['customer'])
		db.session.add(deal)
		db.session.commit()
		return deal, 201

	@marshal_with(resource_fields_deal)
	def delete(self):

		result = DealModel.query.filter_by(id=pc_id).first()
		if not result:
			abort(404, message="Could not find with that ID")
		db.session.delete(result)
		db.session.commit()
		return result

api.add_resource(Deal, '/deal')


if __name__ == '__main__':
	app.run(debug=True)
	